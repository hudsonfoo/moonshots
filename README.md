# Moon Shot (TM)

## Setup

1.  Clone this repo
2.  Run `npm install`
3.  Run `npm start`, **localhost:8080** will open up in your default browser
4.  Run `node server.js`, **localhost:8081** will act as the API server
