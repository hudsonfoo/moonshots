import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import GoogleMapReact from 'google-map-react';
import axios from 'axios';
import moment from 'moment';

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Dropzone from 'react-dropzone';
import AddToQueue from '@material-ui/icons/AddToQueue';

import Satellite from './satellite';
import Barrels from './barrels';

const styles = theme => ({
  moon: {
    width: '40px',
    marginRight: '15px',
  },
  appBar: {
    position: 'relative',
    backgroundColor: '#5E2971',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  updateSatellitesContainer: {
    backgroundColor: '#9B74A9',
    width: '100%',
    height: '100%',
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addToQueueIcon: {
    verticalAlign: 'middle',
    color: '#ffffff',
  },
});

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedSatellite: null,
      satellites: [],
    };
  }

  onDrop(acceptedFiles) {
    let data = new FormData();
    data.append('satellite', acceptedFiles[0]);
    axios
      .post('http://localhost:8081/upload', data)
      .then(res => {
        this.updateSatellites();
      })
      .catch(err => {
        console.warn('ERROR UPLOADING SATELLITE DATA');
      });
  }

  handleSatelliteClick(satellite) {
    if (this.selectedSatellite === satellite) {
      this.setState({selectedSatellite: null});
    } else {
      this.setState({selectedSatellite: satellite});
    }
  }

  updateSatellites() {
    return axios
      .get('http://localhost:8081/data')
      .then(res => {
        this.setState({satellites: res.data});
      })
      .catch(err => {});
  }

  getBarrelStatus(satellite) {
    let status = 'ready';

    if (satellite && satellite.barrels) {
      satellite.barrels.forEach((barrel) => {
        if (barrel.status === 'error') {
          status = 'error';
        }
      });
    }
    return status;
  }

  componentWillMount() {
    this.updateSatellites();
  }

  render() {
    const {classes} = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <img src="moon.png" className={classes.moon} />
            <Typography variant="title" color="inherit" noWrap>
              Moon Shots
              <sup>&trade;</sup>
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container justify="center">
          <Grid item xs={12} sm={11} md={10}>
            <main>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <div style={{height: '50vh', width: '100%'}}>
                    <GoogleMapReact
                      bootstrapURLKeys={{
                        key: 'AIzaSyB2tiHD6eigeNFDW4qmAaxjOSNxw1ZRXAo',
                      }}
                      defaultCenter={this.props.center}
                      defaultZoom={this.props.zoom}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                  <Dropzone
                    onDrop={this.onDrop.bind(this)}
                    activeClassName="active"
                    className={
                      classes.updateSatellitesContainer + ' dropzone-hover'
                    }>
                    <IconButton
                      aria-label="Add Satellite"
                      className={classes.addToQueueIcon}>
                      <AddToQueue />
                    </IconButton>
                  </Dropzone>
                </Grid>
                {this.state.satellites.map((satellite, i) => (
                  <Grid item xs={12} sm={6} md={3} key={i}>
                    <Satellite
                      onClick={this.handleSatelliteClick.bind(this, satellite)}
                      status={this.getBarrelStatus(satellite)}
                      lat="34.12345"
                      lng="-96.12345"
                      title={'Satellite: ' + satellite.satellite_id}
                      date={moment.unix(satellite.telemetry_timestamp).format()}
                    />
                  </Grid>
                ))}
              </Grid>
            </main>
          </Grid>
        </Grid>
        <Barrels selectedSatellite={this.state.selectedSatellite} />
      </React.Fragment>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

App.defaultProps = {
  center: {
    lat: 59.95,
    lng: 30.33,
  },
  zoom: 1,
};

export default withStyles(styles)(App);
