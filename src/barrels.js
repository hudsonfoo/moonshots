import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {Tooltip} from '@material-ui/core';
import Zoom from '@material-ui/core/Zoom';

const styles = theme => ({
  searchField: {
    marginLeft: '20px',
  },
  list: {},
  listItem: {
    backgroundColor: 'purple',
    color: '#ffffff',
    marginBottom: '1px',
  },
  listItemTextPrimary: {
    color: '#ffffff',
  },
  listItemTextSecondary: {
    color: '#ffffff',
    fontWeight: 'bold',
  },
  statusDiv: {
    width: '20px',
    height: '20px',
    backgroundColor: '#ffffff',
    borderRadius: '50%',
  },
  statusready: {
    backgroundColor: 'green',
  },
  statusaging: {
    backgroundColor: 'blue',
  },
  statuserror: {
    backgroundColor: 'red',
  },
});

class Barrels extends React.Component {
  constructor() {
    super();

    this.state = {
      barrels: [],
      orderType: 'idASC',
      searchText: '',
    };

    this.orderBy = this.orderBy.bind(this);
    this.filter = this.filter.bind(this);
  }

  getBarrels() {
    const barrels = this.props.selectedSatellite
      ? this.props.selectedSatellite.barrels
      : [];

    switch (this.state.orderType) {
      case 'idASC':
        return barrels.sort((a, b) => a.barrel_id > b.barrel_id);
        break;

      case 'idDESC':
        return barrels.sort((a, b) => a.barrel_id < b.barrel_id);
        break;

      case 'statusASC':
        return barrels.sort((a, b) => a.status > b.status);
        break;

      case 'statusDESC':
        return barrels.sort((a, b) => a.status < b.status);
        break;

      case 'errorsASC':
        return barrels.sort((a, b) => a.errors.length > b.errors.length);
        break;

      case 'errorsDESC':
        return barrels.sort((a, b) => a.errors.length < b.errors.length);
        break;

      case 'filter':
        return barrels.filter(barrel => {
          return (
            barrel.status
              .toLowerCase()
              .indexOf(this.state.searchText.toLowerCase()) >= 0 ||
            barrel.last_flavor_sensor_result
              .toLowerCase()
              .indexOf(this.state.searchText.toLowerCase()) >= 0 ||
            barrel.errors.some(error => {
              return (
                error
                  .toLowerCase()
                  .indexOf(this.state.searchText.toLowerCase()) >= 0
              );
            })
          );
        });
        break;
    }

    return barrels;
  }

  orderBy(orderType) {
    this.setState({orderType});
  }

  filter(event) {
    this.setState({searchText: event.target.value});
    this.orderBy('filter');
  }

  render() {
    const {classes} = this.props;

    if (this.props.selectedSatellite) {
      return (
        <div>
          <Grid container justify="center">
            <Grid item xs={12} sm={11} md={10}>
              <Button onClick={this.orderBy.bind(this, 'idASC')} variant={ this.state.orderType === 'idASC' ? 'contained' : 'text'} color='primary'>
                Barrel ID <ExpandMoreIcon />
              </Button>
              <Button onClick={this.orderBy.bind(this, 'idDESC')} variant={ this.state.orderType === 'idDESC' ? 'contained' : 'text'} color='primary'>
                Barrel ID <ExpandLessIcon />
              </Button>
              <Button onClick={this.orderBy.bind(this, 'statusASC')} variant={ this.state.orderType === 'statusASC' ? 'contained' : 'text'} color='primary'>
                Status <ExpandMoreIcon />
              </Button>
              <Button onClick={this.orderBy.bind(this, 'statusDESC')} variant={ this.state.orderType === 'statusDESC' ? 'contained' : 'text'} color='primary'>
                Status <ExpandLessIcon />
              </Button>
              <Button onClick={this.orderBy.bind(this, 'errorsASC')} variant={ this.state.orderType === 'errorsASC' ? 'contained' : 'text'} color='primary'>
                Errors <ExpandMoreIcon />
              </Button>
              <Button onClick={this.orderBy.bind(this, 'errorsDESC')} variant={ this.state.orderType === 'errorsDESC' ? 'contained' : 'text'} color='primary'>
                Errors <ExpandLessIcon />
              </Button>
              <TextField
                label='Search'
                value={this.state.searchText}
                onChange={this.filter}
                margin='normal'
                className={classes.searchField}
              />
              <List className={classes.list}>
                {this.getBarrels().map((barrel, i) => (
                  <ListItem
                    button
                    className={classes.listItem}
                    key={barrel.barrel_id}>
                    <ListItemText
                      primary={'Barrel ID: ' + barrel.barrel_id}
                      secondary={
                        barrel.last_flavor_sensor_result.toUpperCase() +
                        ' - ' +
                        barrel.status.toUpperCase()
                      }
                      classes={{
                        primary: classes.listItemTextPrimary,
                        secondary: classes.listItemTextSecondary,
                      }}
                    />
                    <ListItemIcon>
                      <Tooltip
                        title={
                          barrel.errors.length > 0
                            ? barrel.errors.join(', ')
                            : barrel.status.toUpperCase()
                        }
                        placement="left"
                        TransitionComponent={Zoom}>
                        <div
                          className={
                            classes.statusDiv +
                            ' ' +
                            classes['status' + barrel.status]
                          }
                        />
                      </Tooltip>
                    </ListItemIcon>
                  </ListItem>
                ))}
              </List>
            </Grid>
          </Grid>
        </div>
      );
    }

    return <div />;
  }
}

Barrels.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Barrels);
