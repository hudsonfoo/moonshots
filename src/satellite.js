import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FlightLandIcon from '@material-ui/icons/FlightLand';
import BlockIcon from '@material-ui/icons/Block';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';

import Alert from './alert';

const styles = theme => ({
  card: {
    width: '100%',
    backgroundColor: '#431155',
  },
  title: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: '18px',
  },
  subheader: {
    color: '#ffffff',
  },
  actions: {
    display: 'flex',
  },
  icons: {
    color: '#ffffff',
  },
  coords: {
    color: '#ffffff',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif;',
    fontSize: '12px',
    textAlign: 'right',
    display: 'block',
    width: '100%',
  },
  statusready: {
    backgroundColor: 'green',
    width: '20px',
    height: '20px',
  },
  statusaging: {
    backgroundColor: 'blue',
    width: '20px',
    height: '20px',
  },
  statuserror: {
    backgroundColor: 'red',
    width: '20px',
    height: '20px',
  },
  selected: {
    backgroundColor: '#9B74A9',
  },
});

class Satellite extends React.Component {
  constructor() {
    super();
    this.state = {
      selected: false,
      deorbitOpen: false,
      detonateOpen: false,
    };
  }

  handleClickOpen(dialogType) {
    switch (dialogType) {
      case 'deorbit':
        this.setState({deorbitOpen: true});
        break;

      case 'detonate':
        this.setState({detonateOpen: true});
        break;
    }
  }

  handleClickClose(dialogType) {
    switch (dialogType) {
      case 'deorbit':
        this.setState({deorbitOpen: false});
        break;

      case 'detonate':
        this.setState({detonateOpen: false});
        break;
    }
  }

  render() {
    const {classes} = this.props;

    return (
      <Card className={classes.card} onClick={this.props.onClick}>
        <CardHeader
          avatar={
            <Avatar
              aria-label="Status"
              className={classes['status' + this.props.status]}
            />
          }
          action={<IconButton />}
          title={this.props.title}
          subheader={this.props.date}
          classes={{
            title: classes.title,
            subheader: classes.subheader,
          }}
        />
        <CardActions className={classes.actions} disableActionSpacing>
          <Tooltip
            title="Trigger Deorbit Burn"
            placement="top"
            TransitionComponent={Zoom}>
            <IconButton
              onClick={this.handleClickOpen.bind(this, 'deorbit')}
              aria-label="Trigger Deorbit Burn"
              classes={{
                root: classes.icons,
              }}>
              <FlightLandIcon />
            </IconButton>
          </Tooltip>
          <Tooltip
            title="Detonate Satellite"
            placement="top"
            TransitionComponent={Zoom}>
            <IconButton
              onClick={this.handleClickOpen.bind(this, 'detonate')}
              aria-label="Detonate Satellite"
              classes={{
                root: classes.icons,
              }}>
              <BlockIcon />
            </IconButton>
          </Tooltip>
          <span className={classes.coords}>
            {this.props.lat}, {this.props.lng}
          </span>
          <Alert
            title="DETONATE!"
            content="Illegally dispose of a broken satellite in dramatic fashion."
            open={this.state.detonateOpen}
            handleClickClose={this.handleClickClose.bind(this, 'detonate')}
          />
          <Alert
            title="DEORBIT!"
            content="Use the onboard plasma thrusters to slowly ionize some precious (and delicious) ethanol and lower your satellite’s periapsis into the upper atmosphere, starting the process of returning to Earth."
            open={this.state.deorbitOpen}
            handleClickClose={this.handleClickClose.bind(this, 'deorbit')}
          />
        </CardActions>
      </Card>
    );
  }
}

Satellite.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Satellite);
