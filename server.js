const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const port = 8081;
const _ = require('underscore');

let data = [];

// default options
app.use(fileUpload());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/upload', function(req, res) {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  try {
    const satellites = JSON.parse(req.files.satellite.data.toString());

    // Loop through uploaded satellites
    satellites.forEach((satellite) => {
      let existingSatellite = _.findIndex(data, { satellite_id: satellite.satellite_id });

      if (existingSatellite === -1) {
        data.push(satellite);
      } else {
        data[existingSatellite].telemetry_timestamp = satellite.telemetry_timestamp ? satellite.telemetry_timestamp : data[existingSatellite].telemetry_timestamp;

        satellite.barrels.forEach((barrel) => {
          let existingBarrel = _.findIndex(data[existingSatellite].barrels, { barrel_id: barrel.barrel_id });

          if (existingBarrel === -1) {
            data[existingSatellite].barrels.push(barrel);
          } else {
            data[existingSatellite].barrels[existingBarrel] = barrel;
          }
        });
      }
    });
  } catch(e) {
    return res.status(400).send('Invalid JSON', e);
  }

  res.send('File uploaded!');
});

app.get('/data', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(data));
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
